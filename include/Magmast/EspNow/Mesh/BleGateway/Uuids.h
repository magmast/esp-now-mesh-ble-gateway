#ifndef MAGMAST_ESP_NOW_MESH_BLE_GATEWAY_UUIDS_H
#define MAGMAST_ESP_NOW_MESH_BLE_GATEWAY_UUIDS_H

#include <include/etl/string.h>

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            namespace BleGateway
            {
                struct Uuids
                {
                    Uuids();

                    etl::string<36> txService;
                    etl::string<36> txSenderCharacteristic;
                    etl::string<36> txReceiverCharacteristic;
                    etl::string<36> txPayloadCharacteristic;

                    etl::string<36> rxService;
                    etl::string<36> rxSenderCharacteristic;
                    etl::string<36> rxPayloadCharacteristic;
                };
            }
        }
    }
}

#endif