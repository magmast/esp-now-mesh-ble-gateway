#ifndef MAGMAST_ESP_NOW_MESH_BLE_GATEWAY_BLE_GATEWAY_H
#define MAGMAST_ESP_NOW_MESH_BLE_GATEWAY_BLE_GATEWAY_H

#include <EspNowMesh.h>
#include <NimBLEDevice.h>

#include "Uuids.h"

namespace Magmast
{
    namespace EspNow
    {
        namespace Mesh
        {
            namespace BleGateway
            {
                class BleGateway
                {
                private:
                    class ServerCallbacks : public NimBLEServerCallbacks
                    {
                    public:
                        explicit ServerCallbacks(BleGateway &gateway);
                        void onConnect(NimBLEServer *server) override;
                        void onDisconnect(NimBLEServer *server) override;

                    private:
                        BleGateway &_gateway;
                    };

                    class TxSenderCallbacks
                        : public NimBLECharacteristicCallbacks
                    {
                    public:
                        explicit TxSenderCallbacks(BleGateway &gateway);

                        void onWrite(
                            NimBLECharacteristic *characteristic) override;

                    private:
                        BleGateway &_gateway;
                    };

                    class TxPayloadCallbacks
                        : public NimBLECharacteristicCallbacks
                    {
                    public:
                        explicit TxPayloadCallbacks(BleGateway &gateway);

                        void onWrite(
                            NimBLECharacteristic *characterisitc) override;

                    private:
                        BleGateway &_gateway;
                    };

                    struct QueuedPacket
                    {
                        EspNowAddress receiver;
                        std::array<uint8_t, 234> payload;
                    };

                public:
                    BleGateway() = default;
                    explicit BleGateway(const Uuids &&uuids);

                    /// Before calling this method initialize NimBLE stack by
                    /// calling NimBLEDevice::init() and NimBLE::createServer().
                    void begin();

                    void loop();

                private:
                    Uuids _uuids;
                    ServerCallbacks _serverCallbacks{*this};
                    EspNowMeshNode *_node;
                    NimBLECharacteristic *_txSender;
                    TxSenderCallbacks _txSenderCallbacks{*this};
                    NimBLECharacteristic *_txReceiver;
                    TxPayloadCallbacks _txPayloadCallbacks{*this};
                    NimBLECharacteristic *_rxSender;
                    NimBLECharacteristic *_rxPayload;
                    etl::optional<QueuedPacket> _queuedPacket;

                    void handleNodeReceive(
                        const EspNowAddress &sender,
                        const etl::array_view<uint8_t> &payload);
                };
            }
        }
    }
}

#endif