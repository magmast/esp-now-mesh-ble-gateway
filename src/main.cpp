#include <Arduino.h>
#include <EspNowMesh.h>

#include "Magmast/EspNow/Mesh/BleGateway/BleGateway.h"

Magmast::EspNow::Mesh::BleGateway::BleGateway gateway;
// EspNowMeshNode *node;

void setup()
{
  Serial.begin(9600);
  EspNowMesh.begin();

  // node = &EspNowMesh.createNode();

  NimBLEDevice::init("Gateway");
  NimBLEDevice::createServer();
  gateway.begin();
}

// int counter = 80000;

void loop()
{
  // counter--;
  // if (counter <= 0)
  // {
  //   counter = 80000;
  //   std::array<uint8_t, 234> payload{0xFF, 0x00, 0xFF};
  //   node->broadcast(std::move(payload));
  // }
}