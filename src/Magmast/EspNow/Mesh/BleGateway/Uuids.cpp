#include "Magmast/EspNow/Mesh/BleGateway/Uuids.h"

Magmast::EspNow::Mesh::BleGateway::Uuids::Uuids()
    : txService{"58960bd4-3777-4054-9acf-475f89184049"},
      txSenderCharacteristic{"a48e9ad8-f74c-47d1-a5d7-be5f785099f2"},
      txReceiverCharacteristic{"4a514410-0811-4252-837a-b19fa6680808"},
      txPayloadCharacteristic{"89681efa-99a0-4ea1-98de-03a9780d7ca3"},
      rxService{"9a54755b-f03c-446d-b226-ed2d0a6aafb0"},
      rxSenderCharacteristic{"40e1a5ba-60e1-4298-af8a-e828fad03c9c"},
      rxPayloadCharacteristic{"2aab536b-5cae-443b-8e58-6568e15adff3"}
{
}