#include "Magmast/EspNow/Mesh/BleGateway/BleGateway.h"

Magmast::EspNow::Mesh::BleGateway::BleGateway::ServerCallbacks::ServerCallbacks(
    BleGateway &gateway)
    : _gateway{gateway}
{
}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::ServerCallbacks::onConnect(
    NimBLEServer *server)
{
    NimBLEServerCallbacks::onConnect(server);

    auto peer = server->getPeerInfo(0);
    const EspNowAddress address{peer.getAddress().getNative()};
    _gateway._txSender->setValue(address.data(), EspNowAddress::SIZE);
    _gateway._node = &::EspNowMesh.createNode(std::move(address));
    _gateway._node->onReceive(std::bind(
        &BleGateway::handleNodeReceive, &_gateway,
        std::placeholders::_1, std::placeholders::_2));
}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::ServerCallbacks::onDisconnect(
    NimBLEServer *server)
{
    NimBLEServerCallbacks::onDisconnect(server);

    _gateway._node->removeOnReceive();
    ::EspNowMesh.removeNode(*_gateway._node);
    _gateway._node = nullptr;
}

Magmast::EspNow::Mesh::BleGateway::BleGateway::TxSenderCallbacks::TxSenderCallbacks(
    BleGateway &gateway) : _gateway{gateway} {}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::TxSenderCallbacks::onWrite(
    NimBLECharacteristic *characteristic)
{
    const auto data = characteristic->getValue();
    const EspNowAddress address{reinterpret_cast<const uint8_t *>(data.data())};
    _gateway._node->setAddress(std::move(address));
}

Magmast::EspNow::Mesh::BleGateway::BleGateway::TxPayloadCallbacks::TxPayloadCallbacks(
    BleGateway &gateway) : _gateway{gateway} {}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::TxPayloadCallbacks::onWrite(
    NimBLECharacteristic *characterisitc)
{
    NimBLECharacteristicCallbacks::onWrite(characterisitc);

    const auto receiverData = _gateway._txReceiver->getValue();
    const EspNowAddress receiver{reinterpret_cast<const uint8_t *>(receiverData.data())};

    const auto payloadData = characterisitc->getValue();
    std::array<uint8_t, 234> payload;
    std::copy_n(
        reinterpret_cast<const uint8_t *>(payloadData.data()),
        payloadData.size(),
        payload.begin());

    _gateway._queuedPacket = QueuedPacket{
        std::move(receiver), std::move(payload)};
}

Magmast::EspNow::Mesh::BleGateway::BleGateway::BleGateway(const Uuids &&uuids)
    : _uuids{uuids}
{
}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::begin()
{
    const auto server = NimBLEDevice::getServer();
    server->setCallbacks(&_serverCallbacks);

    const auto txService = server->createService(_uuids.txService.c_str());
    _txSender = txService->createCharacteristic(
        _uuids.txSenderCharacteristic.c_str(),
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE);
    _txSender->setCallbacks(&_txSenderCallbacks);
    _txReceiver = txService->createCharacteristic(
        _uuids.txReceiverCharacteristic.c_str(), NIMBLE_PROPERTY::WRITE);
    const auto txPayload = txService->createCharacteristic(
        _uuids.txPayloadCharacteristic.c_str(), NIMBLE_PROPERTY::WRITE);
    txPayload->setCallbacks(&_txPayloadCallbacks);
    txService->start();

    const auto rxService = server->createService(_uuids.rxService.c_str());
    _rxSender = rxService->createCharacteristic(
        _uuids.rxSenderCharacteristic.c_str(),
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
    EspNowAddress defaultAddress;
    _rxSender->setValue(defaultAddress.data(), defaultAddress.SIZE);
    _rxPayload = rxService->createCharacteristic(
        _uuids.rxPayloadCharacteristic.c_str(),
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);
    _rxPayload->setValue(nullptr, 0);
    rxService->start();

    const auto advertising = server->getAdvertising();
    advertising->addServiceUUID(_uuids.txService.c_str());
    advertising->addServiceUUID(_uuids.rxService.c_str());
    advertising->start();
}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::loop()
{
    if (_queuedPacket)
    {
        _node->send(std::move(_queuedPacket->receiver),
                    std::move(_queuedPacket->payload));
        _queuedPacket = etl::nullopt;
    }
}

void Magmast::EspNow::Mesh::BleGateway::BleGateway::handleNodeReceive(
    const EspNowAddress &sender,
    const etl::array_view<uint8_t> &payload)
{
    _rxSender->setValue(sender.data());
    _rxSender->notify();

    _rxPayload->setValue(payload.data(), payload.size());
    _rxPayload->notify();
}